<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['blog'] = 'blog/public_index';
$route['blog/(:any)'] = 'blog/public_show/$1';

$route['admin/login'] = 'auth/login';
$route['admin/logout'] = 'auth/logout';
$route['admin/register'] = 'auth/register';

$route['admin/blog'] = 'blog';
$route['admin/blog/post'] = 'blog/post';
$route['admin/blog/edit/(:num)'] = 'blog/edit/$1';
$route['admin/blog/delete/(:num)'] = 'blog/delete/$1';
$route['admin/blog/trash'] = 'blog/trash';
$route['admin/blog/trash/undo/(:num)'] = 'blog/undo/$1';
$route['admin/api/blog/(:num)']['GET'] = 'api/get_blog_admin/$1';
$route['admin/images']      = 'images/index';
$route['admin/images/add']  = 'images/add';

$route['admin'] = 'admin';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
