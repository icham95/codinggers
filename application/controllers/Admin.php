<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){  
		parent::__construct();
		$this->load->library('Auth_check');
		$this->auth_check->is_logged();
	}

	public function index()
	{
		$this->load->view('templates/admin_header');
		$this->load->view('admin/dashboard');
	}
	
}
