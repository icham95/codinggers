<?php 

class Api extends CI_Controller
{
    public function get_blog_admin($id)
    {
        $this->load->library('auth_check');
        $this->auth_check->is_logged();

        $this->load->model('blog_model');
        $blog = $this->blog_model->show($id);
        $this->response($blog);
    }

    public function response($response, $status = 200)
    {
        $this->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
}
