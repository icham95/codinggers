<?php 

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
    }

    public function logout()
    {
        $this->session->unset_userdata(['username','logged','id']);
        redirect('admin/login');
    }

    public function login()
    {
        $this->load->library('auth_check');
        $this->auth_check->is_logged(1);

        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        if ($this->form_validation->run() == true) {
            $user = $this->check_login();
            $this->session->set_userdata([
               'username' => $user->username,
               'logged'   => true, 
               'id'       => $user->id,
            ]);
            $this->session->set_flashdata('msg_flash', [
                'type'  => 'success',
                'value' => 'welcome ' . $this->session->userdata('username')
            ]);
            redirect('admin');            
        }

        $this->load->view('auth/login');
    }

    public function check_login()
    {
        $user = $this->auth_model->get_where_row(['username' => $this->input->post('username')]);
        if ($user == false) {
            $this->session->set_flashdata('msg_flash', [
                'type' => 'error',
                'value' => 'login failed. user not found'
            ]);
            redirect('admin/login');
        }
        
        if (password_verify($this->input->post('password'), $user->password) == false) {
            $this->session->set_flashdata('msg_flash',[
                'type' => 'error',
                'value' => 'login failed. password mismatch.'
            ]);
            redirect('admin/login');
        }

        return $user;
    }

    public function register()
    {
        $this->load->library('auth_check');
        $this->auth_check->is_logged(1);

        $this->load->model('gender_model');
        $data['data_genders'] = $this->load_gender();

        $this->form_validation->set_rules('email','email', 'required');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('re_password', 're_password', 'required');
        $this->form_validation->set_rules('gender', 'gender','required');

        if ($this->form_validation->run() == true) {
            $this->create();
            $this->session->set_flashdata('msg_flash', [
                'type'  => 'success',
                'value' => 'register success'
            ]);
        }

        $this->load->view('auth/register', $data);        
    }

    public function create()
    {
        $data = [
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => password_hash( $this->input->post('password'), PASSWORD_DEFAULT ),
            'gender_id' => $this->input->post('gender'),
            'status_id' => 1
        ];

        $this->auth_model->create($data);
    }

    public function load_gender()
    {
        return $this->gender_model->index();
    }

}
