<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->model('categorie_model');
		// library buat check login
		$this->load->library('auth_check');
	}

	public function public_index()
	{
		$blogs = $this->blog_model->index();
		if ($blogs != false) {
			foreach ($blogs as $blog) {
				$categories = $this->categorie_model->where_result_cb(['blog_id' => $blog->id]);
				$blog->categories = $categories;
			}
		}
		$data['blogs'] = $blogs;

		$this->load->view('templates/public_header');
		$this->load->view('templates/public_nav');
		$love['title'] = 'We love sharing';
		$love['sub_title'] = 'Knowledge, story, campus and many mores !';
		$this->load->view('templates/iloveyou', $love);
		$this->load->view('blog/index', $data);
	}

	public function public_show($slug)
	{
		$blog = $this->blog_model->show($slug, true);
		$data['categorie'] = $this->categorie_model->show_cb($blog->id);
		$data['blog'] = $blog;

		$this->load->view('templates/public_header');
		$this->load->view('templates/public_nav');
		$this->load->view('blog/show', $data);
	}

	public function index()
	{
		$this->auth_check->is_logged();

		$blogs = $this->blog_model->index();
		if ($blogs != false) {
			foreach ($blogs as $blog) {
				$categories = $this->categorie_model->where_result_cb(['blog_id' => $blog->id]);
				$blog->categories = $categories;
			}
		}
		$data['blogs'] = $blogs;
		
		$this->load->view('templates/admin_header');
		$this->load->view('admin/blog/index', $data);
		$this->load->view('js/showModalAdminBlog');
	}

	public function show()
	{
		
	}

	public function post()
	{
		// cek apakah login
		$this->auth_check->is_logged();
		
		$this->load->model('categorie_model');
		$data['categories'] = $this->categorie_model->index();

		// init validasi
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('body', 'body', 'required');
		// cek validasi
		if ( $this->form_validation->run() == true ) {
			$this->load->helper('url');
			$slug = url_title($this->input->post('title'), 'dash', TRUE);
			// data insert database
			$data_insert = [
				'title' => $this->input->post('title'),
				'slug'  => $slug,
				'body'  => $this->input->post('body'),
				'small_image_id' => $this->input->post('small_image'),
				'big_image_id' => $this->input->post('big_image'),
				'admin_id' => $this->session->userdata('id')
			];
			// masukin ke table blog return id blog
			$blog_id = $this->blog_model->insert($data_insert);
			foreach( $this->input->post('categorie') as $categorie ){
				// insert categorie
				$data_insert_cat = [
					'blog_id' => $blog_id,
					'categorie_id' => $categorie
				];
				$this->categorie_model->insert_cb($data_insert_cat);
			}
			// set flash session , feedback
			$this->session->set_flashdata('msg_flash', [
				'type' => 'success' ,
				'value' => 'post blog success.'
			]);
			// redirect
			redirect('admin/blog/post');
		}
		// tampil
		$this->load->view('templates/admin_header');
		$this->load->view('admin/blog/post', $data);
	}

	public function edit($id)
	{
		$this->auth_check->is_logged();
		$data['blog'] = $this->blog_model->show($id);
		$data['categories_blog'] = $this->categorie_model->show_cb($id);
		$data['categories'] = $this->categorie_model->index();

		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('body', 'body', 'required');
		$this->form_validation->set_rules('small_image', 'small_image', 'required');
		$this->form_validation->set_rules('big_image', 'big_image', 'required');
		if ($this->form_validation->run() == true) {
			// update data
			$this->update($id);
			$this->session->set_flashdata('msg_flash', [
				'type' => 'success',
				'value' => 'update success'
			]);
			redirect('admin/blog/');
		}

		$this->load->view('templates/admin_header');
		$this->load->view('admin/blog/edit', $data);
	}

	public function update($id)
	{
		$date = new DateTime('now', new DateTimeZone('asia/jakarta'));
		$now = $date->format('Y-m-d h:i:s');

		$this->load->helper('url');
		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data_update = [
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'body' => $this->input->post('body'),
			'small_image_id' => $this->input->post('small_image'),
			'big_image_id' => $this->input->post('big_image'),
			'updated_at' => $now,
		];
		$this->blog_model->update($data_update, $id);

		// update categorie blog
		// delete semua
		$this->categorie_model->delete_cb($id);
		// masukin
		foreach ($this->input->post('categorie') as $value) {
			$this->categorie_model->insert_cb(['blog_id' => $id, 'categorie_id' => $value]);
		}
	}

	public function delete($id)
	{
		$this->auth_check->is_logged();
		$date = new DateTime('now', new DateTimeZone('asia/jakarta'));
		$now = $date->format('Y-m-d h:i:s');
		$data_soft_delete = [
			'deleted_at' => $now,
		];
		$this->blog_model->update($data_soft_delete, $id);
		$this->session->set_flashdata('msg_flash', [
			'type' => 'success',
			'value' => "delete $id success"
		]);
		redirect('admin/blog');
	}

	public function trash()
	{
		$this->auth_check->is_logged();

		$data['trash'] = $this->blog_model->trash();
		
		$this->load->view('templates/admin_header');
		$this->load->view('admin/blog/trash', $data);
		$this->load->view('js/showModalAdminBlog');
	}

	public function undo($id)
	{
		$this->auth_check->is_logged();
		$data_undo = [
			'deleted_at' => null
		];
		$this->blog_model->update($data_undo, $id);
		$this->session->set_flashdata('msg_flash', [
			'type' => 'success',
			'value' => 'undo success'
		]);
		redirect('admin/blog/trash');
	}

	public function do_upload($name, $width = 1024, $height = 768)
	{
		// init config
		$config['upload_path'] 	= './images/';
		$config['allowed_types']= 'gif|jpg|png';
		$config['max_size'] 	= 200;
		$config['max_width'] 	= $width;	
		$config['max_height']  	= $height;
		$config['encrypt_name'] = true;
		// load library dari codeigniter, upload
		$this->load->library('upload', $config);
		// mulai upload
		if ( ! $this->upload->do_upload($name) ) {
			// jika gagal
			$this->session->set_flashdata('msg_flash', [
				'type' => 'error',
				'value' => $name . ' (image) failed to upload '
			]);
			redirect('admin/blog/post');
		}
		else {
			// jika berhasil
			// load model images_model
			$this->load->model('images_model');
			// ambil informasi gambar yang di upload
			$data_images = $this->upload->data();
			// data insert ke database
			$data_insert = [
				'path' => $data_images['file_name'],
				'admin_id' => $this->session->userdata('id')
			];
			// masukin ke database dan return id data yang dimasukan ke database
			return $this->images_model->return_id_insert($data_insert);
		}
	}

}
