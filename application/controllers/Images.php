<?php

class Images extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // load model images_model
        $this->load->model('images_model');
    }

    public function index()
    {
        $data['images'] = $this->images_model->index();
        $this->load->view('admin/images/index', $data);
    }
    
    public function add()
    {
        $this->load->library('auth_check');
        $this->auth_check->is_logged();

        $this->form_validation->set_rules('title','title','required');

        if ($this->form_validation->run() == true) {
            $image_id = $this->do_upload('images');
            $this->session->set_flashdata('msg_flash',[
                'type' => 'success',
                'value' => 'upload success',
            ]);
            redirect('admin/images');
        }

        $this->load->view('admin/images/add');
    }

    public function do_upload($name, $width = 1024, $height = 768)
	{
		// init config
		$config['upload_path'] 	= './images/';
		$config['allowed_types']= 'gif|jpg|png';
		$config['max_size'] 	= 200;
		$config['max_width'] 	= $width;	
		$config['max_height']  	= $height;
		$config['encrypt_name'] = true;
		// load library dari codeigniter, upload
		$this->load->library('upload', $config);
		// mulai upload
		if ( ! $this->upload->do_upload($name) ) {
			// jika gagal
			$this->session->set_flashdata('msg_flash', [
				'type' => 'error',
				'value' => $name . ' (image) failed to upload '
			]);
			redirect('admin/blog/post');
		}
		else {
			// jika berhasil
			// ambil informasi gambar yang di upload
			$data_images = $this->upload->data();
			// data insert ke database
			$data_insert = [
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
				'path' => $data_images['file_name'],
				'admin_id' => $this->session->userdata('id')
			];
			// masukin ke database dan return id data yang dimasukan ke database
			return $this->images_model->return_id_insert($data_insert);
		}
	}

}
