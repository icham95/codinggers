<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migrate extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("migration");
    }

    public function index($version){
      if(!$this->migration->version($version)){
          show_error($this->migration->error_string());
      }   
    }

    public function all()
    {
        for($i=1; $i<=7; $i++){
            $this->index($i);
        }
        echo 'tables created <br>';

        $this->load->model('gender_model');
        $this->gender_model->insert(['name' => 'pria']);
        $this->gender_model->insert(['name' => 'wanita']);
        echo 'seeding genders ok <br>';

        $this->load->model('status_model');
        $this->status_model->insert(['name' => 'admin','id' => 1]);
        $this->status_model->insert(['name' => 'null', 'id' => 0]);
        echo 'seeding status ok <br>';

        $this->load->model('categorie_model');
        $this->categorie_model->insert(['name' => 'html']);
        $this->categorie_model->insert(['name' => 'php']);
        $this->categorie_model->insert(['name' => 'css']);
        $this->categorie_model->insert(['name' => 'javascript']);
        echo 'seeding categories ok <br>';
    }
}