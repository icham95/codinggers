<?php 

class Auth_check
{

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function is_logged($feedback = 0)
    {
        // 1 berarti di daerah login.php
        if ($this->CI->session->userdata('logged') == true) {
            if ($feedback == 1) {
                redirect('admin');
            }
        }
        else {
            if ($feedback == 0) {
                redirect('admin/login');
            }
        }
    }
    
}
