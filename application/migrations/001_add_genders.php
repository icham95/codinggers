<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_genders extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'tinyint',
                                'constraint' => 3,
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'varchar',
                                'constraint' => 30,
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('genders');
        }

        public function down()
        {
                $this->dbforge->drop_table('genders');
        }
}