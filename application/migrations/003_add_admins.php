<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_admins extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ),
                        'email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'username' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'fullname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'gender_id' => array(
                                'type' => 'tinyint',
                                'constraint' => '3',
                        ),
                        'image_id' => array(
                                'type' => 'int',
                                'constraint' => '11',
                        ),
                        'created_at' => array(
                                'type' => 'timestamp',
                        ),
                        'updated_at' => array(
                                'type' => 'timestamp',
                                'null' => true,
                        ),
                        'status_id' => array(
                                'type' => 'tinyint',
                                'constraint' => '11',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_key('gender_id');
                $this->dbforge->add_key('status_id');
                $this->dbforge->add_key('image_id');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(gender_id) REFERENCES genders(id)');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(status_id) REFERENCES status(id)');
                $this->dbforge->create_table('admins');
        }

        public function down()
        {
                $this->dbforge->drop_table('admins');
        }
}