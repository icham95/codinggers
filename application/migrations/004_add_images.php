<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_images extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ),
                        'title' => array(
                                'type' => 'varchar',
                                'constraint' => 100,
                        ),
                        'description' => array(
                                'type' => 'text',
                        ),
                        'path' => array(
                                'type' => 'varchar',
                                'constraint' => 255,
                        ),
                        'admin_id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                        ),
                        'created_at' => array(
                                'type' => 'timestamp',
                        ),
                        'updated_at' => array(
                                'type' => 'timestamp',
                                'null'  => true,
                        ),
                ));
                $this->dbforge->add_key('admin_id');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(admin_id) REFERENCES admins(id)');
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('images');
        }

        public function down()
        {
                $this->dbforge->drop_table('images');
        }
}