<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_blogs extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                                'auto_increment' => true,
                        ),
                        'title' => array(
                                'type' => 'varchar',
                                'constraint' => 100,
                        ),
                        'slug' => array(
                                'type' => 'varchar',
                                'constraint' => 255,
                        ),
                        'body' => array(
                                'type' => 'text',
                        ),
                        'admin_id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                        ),
                        'small_image_id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                        ),
                        'big_image_id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                        ),
                        'created_at' => array(
                                'type' => 'timestamp'
                        ),
                        'updated_at' => array(
                                'type' => 'timestamp',
                                'null' => true,
                        ),
                        'deleted_at' => array(
                                'type' => 'timestamp',
                                'null' => true,
                        ),
                        'status_id' => array(
                                'type' => 'tinyint',
                                'constraint' => 11,
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_key('admin_id');
                $this->dbforge->add_key('small_image_id');
                $this->dbforge->add_key('big_image_id');
                $this->dbforge->add_key('status_id');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(admin_id) REFERENCES admins(id)');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(small_image_id) REFERENCES images(id)');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(big_image_id) REFERENCES images(id)');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(status_id) REFERENCES status(id)');
                $this->dbforge->create_table('blogs');
        }

        public function down()
        {
                $this->dbforge->drop_table('blogs');
        }
}