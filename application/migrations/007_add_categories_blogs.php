<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_categories_blogs extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ),
                        'blog_id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                        ),
                        'categorie_id' => array(
                                'type' => 'int',
                                'constraint' => 11,
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_key('blog_id');
                $this->dbforge->add_key('categorie_id');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(blog_id) REFERENCES blogs(id)');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY(categorie_id) REFERENCES categories(id)');
                $this->dbforge->create_table('categories_blogs');
        }

        public function down()
        {
                $this->dbforge->drop_table('categories_blogs');
        }
}