<?php 

class Auth_model extends CI_Model
{
    public function create($data)
    {
        $this->db->insert('admins', $data);
    }

    public function get_where_row($where)
    {
        $this->db->where($where);
        $query = $this->db->get('admins');
        if (empty($query->row())) {
            return false;
        }

        return $query->row();
    }
}
