<?php 

class Blog_model extends CI_Model
{
    public function index()
    { 
        $this->db->select($this->select());
        $this->db->join('admins', 'blogs.admin_id = admins.id');
        $this->db->where([
            'deleted_at =' => null
        ]);
        $query = $this->db->get('blogs');
        if ( ! empty($query->result())) {
            return $query->result();
        }
        return false;
    }

    public function show($key, $slug = false)
    {
        $this->db->select($this->select());
        $this->db->join('admins', 'blogs.admin_id = admins.id');
        if ($slug == false) {
            $this->db->where('blogs.id', $key);
        }
        else {
            $this->db->where('blogs.slug', $key);
        }
        $query = $this->db->get('blogs');
        if ( ! empty($query->row())) {
            return $query->row();
        }
        return false;
    }

    public function insert($data)
    {
        $this->db->insert('blogs', $data);
        return $this->db->insert_id();
    }

    public function update($data, $id)
    {
        $this->db->update('blogs',$data, ['id' => $id]);
    }
    
    public function trash()
    {
        $this->db->select($this->select());
        $this->db->join('admins', 'blogs.admin_id = admins.id');
        $this->db->where('deleted_at !=', null);
        $query = $this->db->get('blogs');
        if ( ! empty($query->result())){
            return $query->result();
        }
        return false;
    }

    public function select()
    {
        return [
            'blogs.id',
            'blogs.title',
            'blogs.slug',
            'blogs.body',
            'blogs.created_at',
            'blogs.updated_at',
            'blogs.deleted_at',
            'admins.username',
            'admins.fullname',
            '(select path from images where images.id = blogs.small_image_id ) as path_small_image',
            '(select path from images where images.id = blogs.big_image_id ) as path_big_image',
            '(select id from images where images.id = blogs.small_image_id ) as small_image_id',
            '(select id from images where images.id = blogs.big_image_id ) as big_image_id'
        ];
    }
}