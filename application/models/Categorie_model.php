<?php 

class Categorie_model extends CI_Model
{
    public function index()
    {
        $query = $this->db->get('categories');
        if ( ! empty($query->result())) {
            return $query->result();
        }
        return false;
    }

    public function show_cb($id_blog)
    {
        $this->db->select(['categories.name','categories.id']);
        $this->db->join('categories', 'categories_blogs.categorie_id = categories.id');
        $this->db->where('blog_id', $id_blog);
        $query = $this->db->get('categories_blogs');
        if ( ! empty($query->result())) {
            return $query->result();
        }
        return false;
    }

    public function delete_cb($id_blog)
    {
        $this->db->where('blog_id', $id_blog);
        $this->db->delete('categories_blogs');
    }

    public function show_cb_row($id_blog)
    {
        $this->db->select(['categories.name','categories.id']);
        $this->db->join('categories', 'categories_blogs.categorie_id = categories.id');
        $this->db->where('blog_id', $id_blog);
        $query = $this->db->get('categories_blogs');
        if ( ! empty($query->row())) {
            return $query->row();
        }
        return false;
    }

    public function insert($data)
    {
        $this->db->insert('categories', $data);
    }

    public function insert_cb($data)
    {
        $this->db->insert('categories_blogs', $data);
    }

    public function where_result_cb($where)
    {
        $this->db->select([
            'categories_blogs.id as cb_id',
            'categories.id as cat_id',
            'categories.name'
        ]);
        $this->db->join('categories', 'categories_blogs.categorie_id = categories.id');
        $this->db->where($where);
        $query = $this->db->get('categories_blogs');
        if ( ! empty($query->result())) {
            return $query->result();
        }
        return false;
    }
}
