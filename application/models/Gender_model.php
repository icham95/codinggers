<?php 

class Gender_model extends CI_Model
{
    public function index()
    {
        $query = $this->db->get('genders');
        return $query->result();
    }

    public function insert($data)
    {
        $this->db->insert('genders', $data);
    }
}
