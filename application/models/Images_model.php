<?php 

class Images_model extends CI_Model
{
    public function index()
    {
        $query = $this->db->get('images');
        if ( ! empty($query->result())) {
            return $query->result();
        }
        return false;
    }
    public function return_id_insert($data)
    {
        $this->db->insert('images', $data);
        return $this->db->insert_id();
    }
}
