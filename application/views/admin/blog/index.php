
<?php if(isset($_SESSION['msg_flash'])): ?>
<?= $_SESSION['msg_flash']['value'] ?>
<?php endif; ?>

<?php if($blogs != false): ?>
<div>
<?php foreach($blogs as $blog): ?>
    <div style="width:200px;">
        <div>
            <img src="<?= base_url('images/').$blog->path_small_image ?>" style="width:60px;">
        </div>
        <div> <?= $blog->title ?> </div>
        <div> <?= $blog->body ?> </div>
        <div> <?= $blog->created_at ?> </div>
        <div> <?= $blog->updated_at ?> </div>
        <div> 
            kategorie : 
        <?php
            if ($blog->categories != false): 
                foreach($blog->categories as $categorie): 
        ?>
            <?= $categorie->name ?>,
        <?php 
                endforeach;
                else:
                echo 'tidak ada kategori';
                endif;
        ?>
        </div>
        <div> 
            <a href="" onclick="showModal(event, <?= $blog->id ?>)"> show </a> | 
            <a href="<?= base_url('admin/blog/edit/'. $blog->id) ?>"> edit </a> | 
            <a href="<?= base_url('admin/blog/delete/'. $blog->id) ?>"> hapus </a> 
        </div>  
    </div>
<?php endforeach; ?>
</div>


<?php else: ?>
<div>
    belum ada blog
</div>
<?php endif;?>