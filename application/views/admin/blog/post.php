<?php if(isset($_SESSION['msg_flash'])): ?>
<?= $_SESSION['msg_flash']['value'] ?>
<?php endif; ?>

<?= form_open() ?>
<div>
    <label for=""> title </label>
    <input type="text" name="title" value="<?= set_value('title') ?>">
    <?= form_error('title'); ?>
</div>

<div>
    <label for=""> body </label>
    <textarea name="body" id="" cols="30" rows="10"><?= set_value('body') ?></textarea>
    <?= form_error('body'); ?>
</div>

<div>
    <label for=""> small image id </label>
    <input type="text" name="small_image">
    <?= form_error('small_image'); ?>
</div>

<div>
    <label for=""> big image id </label>
    <input type="text" name="big_image">
    <?= form_error('big_image'); ?>
</div>

<div id="categorie">

</div>
<a href="" onclick="addCategorie(event)"> tambah kategori </a>

<div>
    <input type="submit" value="POST">
</div>
<?= form_close() ?>

<script>
    let domCategorie = document.getElementById("categorie");
    let categorie = document.getElementById("iCategorie");
    let sourceCategorie = 
    `
    <div>
        <label for=""> kategori </label>
        <select name="categorie[]" id="iCategorie">
        <?php foreach($categories as $categorie): ?>
            <option value="<?= $categorie->id ?>"> <?= $categorie->name ?> </option>
        <?php endforeach;?>
        </select>
    </div>
    `;
    domCategorie.innerHTML += sourceCategorie;

    function addCategorie(e){
        e.preventDefault();
        domCategorie.innerHTML += sourceCategorie;
    }
</script>
