
<?php if(isset($_SESSION['msg_flash'])): ?>
<?= $_SESSION['msg_flash']['value'] ?>
<?php endif; ?>

<h2> Trash </h2>

<?php if($trash != false): ?>
<div>
<?php foreach($trash as $blog): ?>
    <div style="width:200px;">
        <div>
            <img src="<?= base_url('images/').$blog->path_small_image ?>" style="width:60px;">
        </div>
        <div> <?= $blog->title ?> </div>
        <div> <?= $blog->body ?> </div>
        <div> buat : <?= $blog->created_at ?> </div>
        <div> update : <?= $blog->updated_at ?> </div>
        <div> delete : <?= $blog->deleted_at ?> </div>
        <div> 
            <a href="" onclick="showModal(event, <?= $blog->id ?>)"> Show </a> | 
            <a href="<?= base_url('admin/blog/trash/undo/'. $blog->id) ?>"> Undo </a>
        </div>
    </div>
<?php endforeach; ?>
</div>

<?php else: ?>
<div>
    trash kosong
</div>
<?php endif;?>