<?php if(isset($_SESSION['msg_flash'])): ?>
<?= $_SESSION['msg_flash']['value'] ?>
<?php endif; ?>

<h2> images </h2>

<?php if($images != false): ?>
<div>
    <hr>
<?php foreach($images as $image): ?>
    <div style="width:200px;">
        <div>
            <img src="<?= base_url('images/').$image->path ?>" style="width:150px;">
        </div>
        <div>
            id : <?= $image->id ?>
        </div>
        <div>
            <?= $image->title ?>
        </div>
        <div>
            <?= $image->description ?>
        </div>
    </div>
    <hr>
<?php endforeach; ?>
</div>

<?php else: ?>
<div>
    belum ada image
</div>
<?php endif;?>