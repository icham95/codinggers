<?= form_open() ?>
<?php if (isset($_SESSION['msg_flash'])): ?>
<?= $_SESSION['msg_flash']['value'] ?>
<?php endif;?>
<div>
    <label for=""> username </label>
    <input type="text" name="username">
    <?= form_error('username'); ?>
</div>

<div>
    <label for=""> password </label>
    <input type="password" name="password">
    <?= form_error('password'); ?>
</div>

<div>
    <input type="submit" value="login">
</div>

<?= form_close() ?>