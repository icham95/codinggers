<?= form_open() ?>

<?php if(isset($_SESSION['msg_flash'])): ?>
<?= $_SESSION['msg_flash']['value'] ?>
<?php endif;?>
<div>
    <label for="">username</label>
    <input type="text" name="username" value="<?= set_value('username') ?>">
    <?= form_error('username') ?>
</div>

<div>
    <label for="">email</label>
    <input type="text" name="email" value="<?= set_value('email') ?>">
    <?= form_error('email') ?>
</div>

<div>
    <label for="">password</label>
    <input type="password" name="password" value="<?= set_value('password') ?>">
    <?= form_error('password') ?>
</div>

<div>
    <label for="">re_password</label>
    <input type="password" name="re_password" value="<?= set_value('re_password') ?>">
    <?= form_error('re_password') ?>
</div>

<div>
    <label for="">gender</label>
    <select name="gender"> 
    <?php foreach($data_genders as $gender): ?>
        <option value="<?= $gender->id ?>"> <?= $gender->name ?> </option>
    <?php endforeach; ?>
    </select>
    <?= form_error('gender') ?>
</div>

<div>
    <input type="submit" value="register">
</div>

<?= form_close() ?>