
<div class="columns is-desktop" style="background-color:steelblue;">
  <div class="column is-half is-offset-one-quarter" style="padding-top:0px;">
    <?php if($blog != false): ?>
    <div class="card">
        <div class="card-image" style="padding-top:25px;">
            <figure class="image is-2by1">
            <img src="<?= base_url('images/') . $blog->path_big_image ?>" alt="Image">
            </figure>
        </div>
        <div class="card-content">
            <div class="media">
                <div class="media-content" style="text-align:center;">
                    <p class="title is-4">
                        <a href="<?= base_url('blog/') . $blog->slug ?>" style="color:grey;"> 
                            <?= $blog->title ?> 
                        </a>
                    </p>
                    <p class="subtitle is-6">oleh : <?= $blog->username ?></p>
                </div>
            </div>
            <div class="content">
            <?= $blog->body; ?>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div>
        blog tidak ditemukan
    </div>
    <?php endif;?>

  </div>
</div>
