<div id="modal_ab">
    <div id="content_mab">
        <!-- mab = modal admin blog -->
        <a href="" onclick="closeModal(event)" style="position:absolute;right:0px;top:0px;"> CLOSE </a>
        <div style="text-align:center;">
            <img src="" id="img_mab">
        </div>
        <div id="title_mab">
        asd
        </div>
        <div id="body_mab">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci ab deleniti, eos eius delectus earum odit eaque non molestiae magni natus laudantium suscipit impedit laboriosam praesentium reiciendis itaque neque sequi.
        </div>
        <div id="date_mab">
            312 12412 210
        </div>
        <div id="by">
            
        </div>
    </div>
</div>
<style>
    #modal_ab {
        display:none;
        min-width:100%;
        min-height:100%;
        background-color: rgba(0, 0, 0, 0.4);
        position:absolute;
        top:0px;
        left:0px;
    }
    #content_mab {
        position:relative;
        width:70%;
        padding:20px;
        min-height:100px;
        margin:30px auto;
        background-color:snow;
        color:black;
    }
    #title_mab {
        width:80%;
        margin:0 auto;
        text-align:center;
    }
    #body_mab {
        width:80%;
        margin:0 auto;
    }
    #date_mab {
        width:80%;
        margin:0 auto;
    }
    #img_mab {
        max-width:30%;
        margin:0 auto;
    }
    #by {
        max-width:80%;
        margin:0 auto;
    }

</style>

<script>
    function showModal(event ,id){
        event.preventDefault();
        let url = "<?= base_url('admin/api/blog/') ?>";
        let url_img = "<?= base_url('images/') ?>";
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let data = JSON.parse(this.response);
                let title = document.getElementById("title_mab");
                let body = document.getElementById("body_mab");
                let date = document.getElementById("date_mab");
                let img = document.getElementById("img_mab");
                let by = document.getElementById("by");

                title.innerHTML = data.title;
                body.innerHTML = data.body;
                date.innerHTML = "created at : " + data.created_at + " <br> " + "updated_at : " + data.updated_at + " <br> " + "deleted_at : " + data.deleted_at;
                img.src = url_img + data.path_big_image;
                by.innerHTML = 'oleh : ' + data.username;
                
            }
        }
        xhttp.open("GET", url + id);
        xhttp.send();
        document.getElementById("modal_ab").style.display = "block";
    }

    function closeModal(event){
        event.preventDefault();
        document.getElementById("modal_ab").style.display = "none";
    }
</script>